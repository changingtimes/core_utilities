<?php
/**
 * ----------------------------------------------------------------------
 * test program for the core utilities library
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php';

use guardianproject\core_utilities\CountryCode as CountryCode;
use guardianproject\core_utilities\CountryFlag as CountryFlag;
use guardianproject\core_utilities\Cryptor as Cryptor;
use guardianproject\core_utilities\HttpResponse as HttpResponse;
use guardianproject\core_utilities\MIMETYpe as MIMEType;
use guardianproject\core_utilities\PageParser as PageParser;
use guardianproject\core_utilities\QRCode as QRCode;
use guardianproject\core_utilities\RateLimiter as RateLimiter;
use guardianproject\core_utilities\SiteIcon as SiteIcon;
use guardianproject\core_utilities\URL as URL;
use guardianproject\core_utilities\Utilities as Utilities;
use guardianproject\core_utilities\UUID as UUID;

use UtilitiesConfig as Config;
Config::showLogging(false);

$site = 'https://guardianproject.info/';

// make sure we've been initialized before running these tests
if (! file_exists('./.uinit')) {
	print "Utility databases have not been initialized. " . 
		"Please run init.php before running this test\n";
	exit(0);
}

try {

	print "testing core utilities with site $site\n\n";
	
	print "HttpResponse\n------------\n";
	$hr = new HttpResponse();
	print json_encode($hr->urlResponse($site), JSON_PRETTY_PRINT) . "\n";

	print "\nURL\n---\n";
	$uh = new URL();
	$res = $uh->ping($site);
	print json_encode($res, JSON_PRETTY_PRINT) . "\n";
	
	print "\nMIMEType\n--------\n";	
	$mt = new MIMEType();
	$ct = $res['content_type'];
	print $site . " presents content type [" . $ct . "] (" . $mt->getApplicationForType($ct) . "); file extension: " . $mt->getFileExtensionForType($ct) . "\n";

	print "\nPageParser\n----------\n";
	$pp = new PageParser();
	$pp->setUrl($site);
	$res = $pp->acquire();
	$data = json_decode($pp->asJson(), true);
	$data['body'] = '...';  // no need to show the entire page body in this test
	print json_encode($data, JSON_PRETTY_PRINT) . "\n";

	print "\nQRCode\n------\n";
	$qr = new QRCode();
	$qr->setDirectory('./test');
	$qr->setFile('gp');
	$qr->setSizeInPixels(300);
	$qr->setFormat('png');
	$qr->setData($site);

	$qr->generateQRCode();
	print "QRCode for site [" . $site . "] written to ./test/gp.png\n";

	print "\nSiteIcon (verbose mode)\n-----------------------\n";
	$si = new SiteIcon();
	$si->setVerboseMode(true);
	print "icon URL: " . $si->findIconUrl($site) . "\n";
	print "icon: " . substr($si->acquireIcon($site), 0, 80) . "...\n";
	if ($si->iconExists($site)) {
		print "site icon for $site is stored locally\n";
	}
	
	print "\nUUID\n----\n";
	print "v3 (full) " . UUID::v3(UUID::v4(), 'test') . "\n";
	print "v3        " . UUID::v3() . "\n";
	print "v5 (full) " . UUID::v5(UUID::v4(), 'test') . "\n";
	print "v5        " . UUID::v5() . "\n";
	print "v4        " . UUID::v4() . "\n";
	print "next      " . UUID::next() . "\n";
	print "next      " . UUID::next() . "\n";	
	print "next      " . UUID::next() . "\n";

	print "\nCryptor\n-------\n";
	
	$crypt = new Cryptor();
	$blob = 'Guardian Project Simple Symmetric Encryptor';
	print "encryption input: " . $blob . "\n";
	$crypt->setChunkMode(true);
	
	print "encrypt/decrypt a string\n";
	$eout = $crypt->encrypt($blob);
	$dout = $crypt->decrypt($eout);
	print "encrypted/decrypted output: " . $dout . "\n";

	print "encrypt/decrypt a file\n";
	file_put_contents('a1b2c3d4', $blob);
	$of = $crypt->encryptFile('a1b2c3d4'); // return encrypted-file's name
	$dout = $crypt->decryptFile($of);
	print "encrypted/decrypted output: " . $dout . "\n";
	unlink($of);
	unlink('a1b2c3d4');
	
	print "encrypt-to/decrypt-from a file\n";
	$crypt->encryptToFile('a1b2c3d4', $blob);
	$dout = $crypt->decryptFile('a1b2c3d4');
	print "encrypted/decrypted output: " . $dout . "\n";
	unlink('a1b2c3d4');

	print "\nCountryCode\n--------\n";
	$cc = new CountryCode(false);
	
	$data = $cc->getCountryDataUsingAlpha3Code('FRA');
	print "Country Data using Alpha3:\n" . json_encode($data, JSON_PRETTY_PRINT) . "]\n";

	$data = $cc->getCountryDataUsingNumericCode('250');
	print "Country Data using Numeric Code:\n" . json_encode($data, JSON_PRETTY_PRINT) . "]\n";

	$codes = $cc->retrieveCountryCodeFuzzy('ite');
	print "Countries with 'ite' in their names:\n" . json_encode($codes, JSON_PRETTY_PRINT) . "\n";	

	print "Independent Nations:\n";
	$all_cc = $cc->allCountryCodes();
	foreach($all_cc as $nc) {
		if ($cc->isIndependent($nc)) {
			$info = $cc->getCountryData($nc);
			print "alpha2 [" . $info['iso3166_a2'] . "] alpha3 [" . $info['iso3166_a3'] . 
				"] numeric [" . $info['iso3166_num'] . "] name: " . $info['iso3166_name']. "\n";
		}
	}

	print "\nNations that are Territories:\n";
	$terr = $cc->allTerritories();
	foreach($terr as $t) {
		$tname = $cc->getCountryName($t);
		$ccode = $cc->territoryOf($t);
		$cname = $cc->getCountryName($ccode);
		print "[$t] $tname is a territory of [$ccode] $cname\n";
	}
	
	print "\nCountryFlag (and RateLimiter)\n--------\n";
	$cf = new CountryFlag(false);
	$cf->updateFlagIcon('UN');
	$iso = $cc->allCountryCodes();
	
	$html = "<html>\n<body>\n<h3>Flags of Some (Random) Nations</h3>\n";
	
	// rate-limit this query
	$throttler = new RateLimiter(true);  // turn it ON
	$throttler->setMaxCallsPerSecond(8); // set rate limit

	foreach ($iso as $c2code) {
		// deliver only a random subset of ~50 flags
		$ran = mt_rand(10356, 3264765);
		if (($ran % 5) != 0) { continue; }

		$data = $cc->getCountryData($c2code);
		$name = $data['iso3166_name'];
		$emoji = $cf->getEmojiForCountryCode($c2code);
		$html .= "<h4>" . $name . " " . $emoji . "</h4>\n";
		$ibits = $cf->getFlagForCountryCode($c2code, 'icon');
		$iimg = '<img src="data:image/png;base64,' . $ibits . '" alt="' . $name . '" />';

		print "retrieving mini-sized flag for [$c2code]\n";
		$mbits = $cf->getFlagForCountryCode($c2code, 'mini');
		$mimg = '<img src="data:image/png;base64,' . $mbits . '" alt="' . $name . '" />';
		
		$html .= "<ul><li>icon " . $iimg . "</li><li>mini " . $mimg . "</li></ul>\n";

		$throttler->throttle();
	}

	$html .= "</body></html>\n";
	file_put_contents('./test/flags.html', $html);
	print "\nopen file ./test/flags.html in your browser to see a selection of flags\n";

} catch (Exception $e) {
	print "Error: " . $e->getMessage() . "\n";
}

exit(0);
?>
