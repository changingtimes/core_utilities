<?php
/**
 * ----------------------------------------------------------------------
 * initialize object databases from source data in CSV files in the data directory
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

date_default_timezone_set('UTC');
require_once './inc.php';

use guardianproject\core_utilities\CountryCode as CountryCode;
use guardianproject\core_utilities\CountryFlag as CountryFlag;
use guardianproject\core_utilities\Cryptor as Cryptor;
use guardianproject\core_utilities\HttpResponse as HttpResponse;
use guardianproject\core_utilities\UUID as UUID;
use guardianproject\core_utilities\MIMEType as MIMEType;

use UtilitiesConfig as Config;

// make sure we have all our directories (as defined in our config)
$dbd = UtilitiesConfig::databaseDirectory();
if (! file_exists($dbd)) {
	mkdir($dbd, 0777, true);
	print "created database directory [$dbd]\n";
}
$dd = UtilitiesConfig::dataDirectory();
if (! file_exists($dd)) {
	mkdir($dd, 0777, true);
	print "created database directory [$dd]\n";
}
$ld = UtilitiesConfig::logDirectory();
if (! file_exists($ld)) {
	mkdir($ld, 0777, true);
	print "created database directory [$ld]\n";
}
$udbd = UtilitiesConfig::utilitiesDbDirectory();
if (! file_exists($udbd)) {
	mkdir($udbd, 0777, true);
	print "created utilities database directory [$udbd]\n";
}
$udd = UtilitiesConfig::utilitiesDataDirectory();
if (! file_exists($udd)) {
	mkdir($udd, 0777, true);
	print "created utilities database directory [$udd]\n";
}

try {
	print "initializing CountryCode database\n";
	$cc = new CountryCode(true);
	print "initializing CountryFlag database\n";
	$cf = new CountryFlag(true);
	$cf->updateFlagIcon('UN');
	print "initializing HTTP response database\n";
	$hr = new HttpResponse(true);
	print "initializing MIMEType database\n";
	$mt = new MIMEType(true);

	if (! file_exists(Config::encryptionKeyFile())) {		
		if (! file_exists(Config::privDirectory())) {
			mkdir(Config::privDirectory());
			chmod(Config::privDirectory(), 0700);
			print "created private directory [" . Config::privDirectory() . "]\n";
		}
		
		print "generating a new encryption key in file '" . Config::encryptionKeyFile() . "'\n";
		Cryptor::generateKey(UUID::next());
	}
	
	// leave a marker indicating the utilities databases where initialized
	file_put_contents('./.uinit', "initialized on " . date('Y-m-d'));
} catch (Exception $e) {
	print "initialization exception: " . $e->getMessage() . "\n";
}

exit(0);
?>
