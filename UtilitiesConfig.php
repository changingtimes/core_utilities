<?php
/**
 * ----------------------------------------------------------------------
 * component: UtilitiesConfig
 * defined constants for the Guardian Project Core Utilities library
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

//NOTE BENE: This class is NOT namespaced

class UtilitiesConfig {

	// directory definitions (not used directly: see static methods below)

	const DB_DIRECTORY    = '/db'; 		// where YOUR SQLITE databases will reside
	const DATA_DIRECTORY  = '/data';	// where YOUR raw data to build our databases resides
	const LOG_DIRECTORY   = '/logs';	// where YOU'LL log
	const PRIV_DIRECTORY = '.priv';		// Where YOUR private data resides

	// this is where the Utilities-owned databases and data are located (not yours)
	const UT_DB_DIRECTORY    = './db';
	const UT_DATA_DIRECTORY  = './data';
	
	// logging definitions
	const LOGFILE         = 'library.log'; 
	const IS_LOGGING      = true; // turn logging on/off
    //const LOG_LEVEL       = 1039; // E_ERROR | E_WARNING | E_PARSE | E_NOTICE | E_USER_NOTICE
    const LOG_LEVEL       = 15;  // E_ERROR | E_WARNING | E_PARSE | E_NOTICE
    	
	// encryption-related
	const KEY_FILE         = '.BOGUS';
	const ENC_ALGORITHM    = 'aes-256-cbc';	
	
	// User-Agent string for curl()
	const DEFAULT_USER_AGENT  = 'Mozilla/5.0 (X11; Linux i686; rv:95.0) Gecko/20100101 Firefox/95.0';
	
	// directory methods - DO NOT EDIT
	public static function databaseDirectory() { return __DIR__ . self::DB_DIRECTORY; }
	public static function dataDirectory() { return __DIR__ . self::DATA_DIRECTORY; }
	public static function logDirectory() { return __DIR__ . self::LOG_DIRECTORY; }
	public static function privDirectory() { return self::PRIV_DIRECTORY; } 
	public static function encryptionKeyFile() { return self::PRIV_DIRECTORY . '/' . self::KEY_FILE; }
	
	public static function utilitiesDbDirectory() { return self::UT_DB_DIRECTORY; }
	public static function utilitiesDataDirectory() { return self::UT_DATA_DIRECTORY; }
	
	public static function isLogging() {
		if (self::IS_LOGGING == 'true') { return true; }
		return false;
	}
	
	public static function showLogging($quiet = true){
		if (self::IS_LOGGING == 'true') {
			print "logging [ON]; messages to file " . self::LOG_DIRECTORY . '/' . self::LOGFILE . "\n";
		} else {
			if (! $quiet) { print "logging [OFF]\n"; }
		}
	}

/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}
?>
