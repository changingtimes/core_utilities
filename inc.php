<?php
/**
 * ----------------------------------------------------------------------
 * all necessary objects for this library
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

// un-namespaced configuration classes we need to pull in manually
require_once './UtilitiesConfig.php';

require_once './vendor/autoload.php';
?>
