<?php
/**
 * ----------------------------------------------------------------------
 * component: RateLimiter
 * The providers of many services ban consumers from executing too many
 * requests in a given time period. This class "throttles" such API
 * calls in a simple way.  
 * NOTE: The first call to this object's pause() method is not penalized.
 * The pause() algorithm generates a list of "spoilers" - where a large
 * penalty is inserted (1-3 seconds) to further limit the access.  These
 * occur "rarely" relative to the MaxCalls rate, though.
 *   
 * Usage:
 * $throttler = new RateLimiter(true);  // turn it ON
 * $throttler->setMaxCallsPerSecond(5); // set rate limit
 * 
 * $huge = big_long_list_of_requests(); //...your code
 * foreach ($huge as $one) {
 *    $result = $this->my_retriever($one); // your action method
 * }
 *
 * private function my_retriever($data) {
 *    if ($this->throttler->isThrottling()) { $this->throttler->throttle(); }
 *    //...
 *    //... your code here
 *    //...
 * }
 *
 * Throttling is OFF by default.
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\core_utilities;

class RateLimiter {

	private $toggle;
	private $max_per_sec = 60;
	private $n_reqs = 0;
	private $t_init;
	private $spoilers;
	
	// ----------------------------------------------------------------------
	// constructor
	// ----------------------------------------------------------------------

	public function __construct($on = false) {
		$this->t_init  = time();
		$this->n_reqs  = 0;
		
		$this->toggle = $on;
		if ($on) { $this->make_spoilers(); }
	}	

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// setters
	// ----------------------------------------------------------------------

	public function setMaxCallsPerSecond($num) { 
		$this->max_per_sec = $num;
	}
	
	public function reinitializeTime() { $this->t_init = time(); }

	// ----------------------------------------------------------------------
	// isThrottling
	// ----------------------------------------------------------------------

	public function isThrottling() { return $this->toggle; }
	
	// ----------------------------------------------------------------------
	// throttle
	// ----------------------------------------------------------------------
	
	public function throttle() { 
		if ($this->toggle) { $this->pause(); } 
		
		return true;
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// make_spoilers
	// ----------------------------------------------------------------------

	private function make_spoilers() {
		$spoilers = array();
		for ($i = 0; $i < 10; $i++) { 
			$spoilers[] = mt_rand(10, 224);
			usleep(mt_rand(10000, 23456));
		}
		
		sort ($spoilers, SORT_NUMERIC);
		$this->spoilers = $spoilers;
		return true;
	}
	
	// ----------------------------------------------------------------------
	// pause - manages all aspects of throttling
	// ----------------------------------------------------------------------

	private function pause() {
		
		// set the base delay or a "spoiler" (but be fair to one-trippers)
		if ($this->n_reqs == 0) { $delay = 0; }
		else if (in_array($this->n_reqs, $this->spoilers)) { $delay = mt_rand(1000000, 3000000); } 
		else { $delay = mt_rand(15000, 55000); }
		
		// check that we've not violated the limit; penalize if so
		$elapsed_m = $this->elapsed();
		if (($elapsed_m > 0) && ($elapsed_m < 1000000)) {
			if ($this->n_reqs >= ($this->max_per_sec - 2)) {
				// wait it out to avoid being banned!
				$buffer = 6100000 - $elapsed_m;
				Utilities::logger('Too many API requests! Pausing for roughly ' . $buffer . ' seconds at ' . date('H:i:s'), E_USER_NOTICE);
				$delay = mt_rand($buffer, $buffer + 4000000);
			}
		} else {
			$now = time();
			Utilities::logger($this->n_reqs . ' requests in last ' . ($now - $this->t_init) . ' seconds. resetting at ' . date('H:i:s'), E_USER_NOTICE);
			$this->n_reqs = 0;
			$this->t_init = $now;
		}
		
		usleep($delay);
		$this->n_reqs++; 
	}

	// ----------------------------------------------------------------------
	// elapsed - return the number of microseconds since initialization time
	// ----------------------------------------------------------------------

	private function elapsed() {
		list($msec, $sec) = explode(' ', microtime(false));
		$mshift = intval($msec * 1000000);
		$shift = $sec - $this->t_init;
		
		$diff = ($shift * 1000000) + $mshift;

		return $diff;
	}
	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
?>
