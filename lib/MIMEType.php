<?php
/**
 * ----------------------------------------------------------------------
 * component: MIMEType
 * See: https://www.iana.org/assignments/media-types/media-types.xhtml
 * See: https://github.com/lettucebo/MimeTypeMap.List/blob/master/src/MimeTypeMap.List/MimeMapping.cs
 * See: https://gist.github.com/nimasdj/801b0b1a50112ea6a997
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\core_utilities;

use \Exception as Exception;
use \PDO as PDO;
use \PDOException as PDOException;
use guardianproject\core_utilities\Utilities;
use guardianproject\core_utilities\URL;
use UtilitiesConfig as Config;

class MIMEType extends DataStore {
	
	public function __construct($force = false) {
		$this->builds_from_source = false;
		
		$this->info_database = 'auxiliary_info.db';
		$this->static_data   = 'mime_types.csv';
		$this->table_name    = 'mime_types';
	
		$this->db_loc   = Config::utilitiesDbDirectory() . '/' . $this->info_database;
		$this->data_loc = Config::utilitiesDataDirectory() . '/' . $this->static_data;

		if (! $this->initialize($force)) {
			throw new Exception('Object creation failed. Database ' . $this->info_database . ' corrupt or incomplete');
		}
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// getFileExtensionForType
	// ----------------------------------------------------------------------

	public function getFileExtensionForType($mime) {
		$val = $this->retrieve_field('file_extension', 'mime_type', $mime);
		return substr($val, 1);
	}

	// ----------------------------------------------------------------------
	// getTypeforFileExtension
	// ----------------------------------------------------------------------
	
	public function getTypeforFileExtension($fext) {
		if (substr($fext,0,1) != '.') { $fext = '.' . $fext; }
		return $this->retrieve_field('mime_type', 'file_extension', $fext);
	}

	// ----------------------------------------------------------------------
	// getApplicationForType
	// ----------------------------------------------------------------------
	
	public function getApplicationForType($mime) {
		return $this->retrieve_field('application', 'mime_type', $mime);
	}

	// ----------------------------------------------------------------------
	// isSimpleContentType (is file with extension X an "image" or "music" or other
	// ----------------------------------------------------------------------
	
	public function isSimpleContentType($simple_type, $fext) {
		if (substr($fext, 0, 1) != '.') { $fext = '.' . $fext; }
		
		$fe_list = $this->retrieve_file_extensions_for_type($simple_type);
		if (in_array($fext, $fe_list)) { return true; }
		return false;
	}

	// ----------------------------------------------------------------------
	// dump
	// ----------------------------------------------------------------------

	public function dump($to_file = null) {
		if ($to_file == null) {
			$ran = substr(sha1(time()), 0, 12);
			$to_file = tempnam(sys_get_temp_dir(), 'MIME_backup_' . $ran);
		}
		$fp = fopen($to_file, "w");
		if (! $fp) {
			Utilities::logger("dump: could not open " . $to_file . " for writing", E_WARNING);
			return false;
		}	

		fputcsv($fp, array('MIME Type', 'File Extension', 'Application'));	// write CSV header
		
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    	while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    		fputcsv($fp, array(
	    			$res['mime_type'],
	    			$res['file_extension'],
	    			$res['application']
	    		));
	    	}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error dumping MIME data (' . $e->getMessage() . ')', E_ERROR);
			return false;
		}		

		fclose($fp);
		Utilities::logger("contents of " . $this->table_name . " dumped to " . $to_file, E_NOTICE);		
		return true;
	}
		
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	
	// ----------------------------------------------------------------------
	// retrieve_file_extensions_for_type
	// ----------------------------------------------------------------------

	private function retrieve_file_extensions_for_type($simple_type) {
		$sql = 'SELECT file_extension FROM ' . $this->table_name . ' WHERE application LIKE \'%' . $simple_type . '%\';';
	    Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);

	    $output = array(); 
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    	while(($res = $q->fetch(PDO::FETCH_ASSOC)) != null) { $output[] = $res['file_extension']; }
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving entries matching field [' . $test_field . '] and [' . $test_value . '] ' . "error: " . $e->getMessage(), E_USER_NOTICE);
			return null;
		}
		
		return $output;
	}	

	// ----------------------------------------------------------------------
	// retrieve_field
	// ----------------------------------------------------------------------

	private function retrieve_field($target_field, $test_field, $test_value) {
		$sql = 'SELECT ' . $target_field . ' FROM ' . $this->table_name . ' WHERE ' . $test_field . '=?;';
	    Utilities::logger("PDO query: " . $sql . ' with ' . json_encode(array($target_field, $test_field, $test_value)), E_USER_NOTICE);
	     
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute(array($test_value));
	    	$res = $q->fetch(PDO::FETCH_ASSOC);
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving entries matching field [' . $test_field . '] and [' . $test_value . ']', E_USER_NOTICE);
			return null;
		}
		
		if (! $res) { return null; }
		return $res[$target_field];
	}	

	// ----------------------------------------------------------------------
	// create_id - create a unique identifier
	// ----------------------------------------------------------------------	
	
	private function create_id($text) {
		return hash('md5', $text);
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// protected methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	
	// ----------------------------------------------------------------------
	// load_table
	// ----------------------------------------------------------------------

	protected function load_table() {
		$fdev = fopen($this->data_loc, 'r');
		if (! $fdev) {
			Utilities::logger("error opening static http data file " . $this->static_data, E_ERROR);
			return false;
		}
		if (! $this->unload_table()) {
			return false;
		}
		
		$num = 0;		
		$vals = fgetcsv($fdev, 200, ','); // strip header
		while ($vals = fgetcsv($fdev, 200, ',')) {
			$this->load_entry(trim($vals[0]), trim($vals[1]), trim($vals[2]));
			$num++;		
		}
		
		Utilities::logger($num . ' entries added to MIME database', E_NOTICE);
		
		fclose($fdev);
		return true;
	}
	
	// ----------------------------------------------------------------------
	// load_entry
	// ----------------------------------------------------------------------

	protected function load_entry($type, $fext, $application) {
	    $sql = 'INSERT OR REPLACE INTO ' . $this->table_name . ' VALUES (?,?,?,?);';
	    Utilities::logger("PDO stmt: " . $sql, E_USER_NOTICE);
	    
	    $id = $this->create_id($type . $fext . $application);
	    try {
			$q = $this->db->prepare($sql);
			$q->execute(array($id, $type, $fext, $application));
		} catch (PDOException $e) {
			Utilities::logger('PDO: load_entry failed to insert values (' . $e->getMessage() . ')', E_ERROR);      
			return false;
		}
		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// create_table
	// ----------------------------------------------------------------------

	protected function create_table() {
		$sql = 'CREATE TABLE ' . $this->table_name . ' (id TEXT PRIMARY KEY, mime_type TEXT, file_extension TEXT, application TEXT);';
		Utilities::logger("PDO stmt: " . $sql, E_NOTICE);
		
		try {
			$this->db->exec($sql);
		} catch (PDOException $e) {
	    	Utilities::logger("PDO error creating database table in " . $this->db_loc . ' is ' . $e->getMessage(), E_ERROR);
	    	return false;
	    }
	    
	    return true;
	}
	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
?>