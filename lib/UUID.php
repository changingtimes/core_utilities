<?php
/**
 * ----------------------------------------------------------------------
 * component: UUID (RFC 4122 COMPLIANT Universally Unique IDentifier) 
 * See
 *    http://php.net/manual/en/function.uniqid.php (Author: Andrew Moore)
 *    https://gist.github.com/dahnielson/508447 (another implementation)
 *    https://tools.ietf.org/html/rfc4122 (the requirements)
 * 
 * The following class generates VALID RFC 4122 COMPLIANT 
 * Universally Unique IDentifiers (UUID) version 3, 4 and 5. Version 3 and
 * 5 UUIDs are named based. They require a namespace (another valid UUID) 
 * and a value (the name). Given the same namespace and name, the output 
 * is always the same. Version 4 UUIDs are pseudo-random.
 *
 * UUIDs generated below validates using OSSP UUID Tool, and output for 
 * named-based UUIDs are exactly the same. This is a pure PHP implementation.
 * 
 * Usage:
 * Named-based UUID
 *
 * (full control over namespace and name parameters)
 * $v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
 * $v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf', 'SomeRandomString');
 *
 * (OR...to use time signature as the randomizer)
 * $v3uuid = UUID::v3('1546058f-5a25-4334-85ae-e68f2a44bbaf');
 * $v5uuid = UUID::v5('1546058f-5a25-4334-85ae-e68f2a44bbaf'); 
 * 
 * (OR...to use time signature and random namespace)
 * $v3uuid = UUID::v3();  // const OID as namespace
 * $v5uuid = UUID::v5();  // const OID as namespace
 *
 * Pseudo-random UUID
 * $v4uuid = UUID::v4();
 *
 * (OR...use any scheme)
 * $any_uuid = UUID::next();
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\core_utilities;

class UUID {

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public static methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// generate an (any valid) identifier
	// ----------------------------------------------------------------------

	public static function next() {
		list ($msec, $sec) = explode(' ', microtime());
		$n = intval(substr($msec, 2)) % 3;
		switch ($n) {
			case 0:
				$uuid = self::v3();
				break;
			case 1:
				$uuid = self::v4();
				break;
			default:	
			case 2:
				$uuid = self::v5();
				break;
		}
		
		return $uuid;
	}

	// ----------------------------------------------------------------------
	// generate a Version 3 identifier
	// ----------------------------------------------------------------------
		
	public static function v3($namespace = null, $name = null) {
	
		if (! $namespace) { $namespace = self::random_namespace(); }
		
    	if(!self::is_valid($namespace)) return false;
    	
    	if (! $name) { $name = self::random_string(); }

    	// Get hexadecimal components of namespace
    	$nhex = str_replace(array('-','{','}'), '', $namespace);

    	// Binary Value
    	$nstr = '';

    	// Convert Namespace UUID to bits
   	 	for($i = 0; $i < strlen($nhex); $i+=2) {
      		$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    	}

    	// Calculate hash value
    	$hash = md5($nstr . $name);

    	return sprintf('%08s-%04s-%04x-%04x-%12s',

      		// 32 bits for "time_low"
      		substr($hash, 0, 8),

      		// 16 bits for "time_mid"
      		substr($hash, 8, 4),

      		// 16 bits for "time_hi_and_version",
      		// four most significant bits holds version number 3
      		(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x3000,

      		// 16 bits, 8 bits for "clk_seq_hi_res",
      		// 8 bits for "clk_seq_low",
      		// two most significant bits holds zero and one for variant DCE1.1
      		(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      		// 48 bits for "node"
      		substr($hash, 20, 12)
   		);
   	}

	// ----------------------------------------------------------------------
	// generate a Version 4 identifier
	// ----------------------------------------------------------------------
	
	public static function v4() {
    	return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

      		// 32 bits for "time_low"
      		random_int(0, 0xffff), random_int(0, 0xffff),

      		// 16 bits for "time_mid"
      		random_int(0, 0xffff),

      		// 16 bits for "time_hi_and_version",
      		// four most significant bits holds version number 4
      		random_int(0, 0x0fff) | 0x4000,

      		// 16 bits, 8 bits for "clk_seq_hi_res",
      		// 8 bits for "clk_seq_low",
      		// two most significant bits holds zero and one for variant DCE1.1
			random_int(0, 0x3fff) | 0x8000,

      		// 48 bits for "node"
      		random_int(0, 0xffff), random_int(0, 0xffff), random_int(0, 0xffff)
    	);
	}

	// ----------------------------------------------------------------------
	// generate a Version 5 identifier
	// ----------------------------------------------------------------------
	
  	public static function v5($namespace = null, $name = null) {
  	
  		if (! $namespace) { $namespace = self::random_namespace(); }
  			
    	if(!self::is_valid($namespace)) return false;

    	if (! $name) { $name = self::random_string(); }
    	
    	// Get hexadecimal components of namespace
    	$nhex = str_replace(array('-','{','}'), '', $namespace);

    	// Binary Value
    	$nstr = '';

    	// Convert Namespace UUID to bits
    	for($i = 0; $i < strlen($nhex); $i+=2) {
      		$nstr .= chr(hexdec($nhex[$i].$nhex[$i+1]));
    	}

    	// Calculate hash value
    	$hash = sha1($nstr . $name);

    	return sprintf('%08s-%04s-%04x-%04x-%12s',

      		// 32 bits for "time_low"
      		substr($hash, 0, 8),

      		// 16 bits for "time_mid"
      		substr($hash, 8, 4),

 	     	// 16 bits for "time_hi_and_version",
      		// four most significant bits holds version number 5
      		(hexdec(substr($hash, 12, 4)) & 0x0fff) | 0x5000,

      		// 16 bits, 8 bits for "clk_seq_hi_res",
      		// 8 bits for "clk_seq_low",
      		// two most significant bits holds zero and one for variant DCE1.1
      		(hexdec(substr($hash, 16, 4)) & 0x3fff) | 0x8000,

      		// 48 bits for "node"
      		substr($hash, 20, 12)
    	);
	}
	
	// ----------------------------------------------------------------------
	// is_valid - check for valid format
	// ----------------------------------------------------------------------
	
  	public static function is_valid($uuid) {
    	return preg_match('/^\{?[0-9a-f]{8}\-?[0-9a-f]{4}\-?[0-9a-f]{4}\-?'.
        			'[0-9a-f]{4}\-?[0-9a-f]{12}\}?$/i', $uuid) === 1;
  	}
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private static methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------  	

  	// ----------------------------------------------------------------------
  	// generate a random namespace (v4 is a find "seed" for v3 or v5)
	// ----------------------------------------------------------------------	
  	
  	private static function random_namespace() {
  		return self::v4();						  		
  	}
  	
	// ----------------------------------------------------------------------
	// generate a random string
	// ----------------------------------------------------------------------
	
  	private static function random_string() {
  		return sha1(microtime());
  	}

  	private static function x() {
  		return self::v5();
  	}
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}	
?>