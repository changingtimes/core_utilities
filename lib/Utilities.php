<?php
/**
 * ----------------------------------------------------------------------
 * component: General utility functions used across many objects.
 * 
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\core_utilities; 
use UtilitiesConfig as Config;

class Utilities {

	// ----------------------------------------------------------------------
	// email an error message and exit
	// ----------------------------------------------------------------------

	public static function mail_and_exit($message, $subject = 'Administrative Error Message') {
		$to = Config::SERVER_ADMIN;
	
		if (Config::IS_MAILING) {
			if (! mail($to, $subject, $message)) {
				Utilities::logger('Could not send email message', E_ERROR);
			}
		}
		exit(0);
	}

	// ----------------------------------------------------------------------
	// Format a flat JSON string to make it more human-readable
	// ----------------------------------------------------------------------
	
	public static function json_format($json) {
		throw new Exception('json_format: please use JSON_PRETTY_PRINT argument to json_encode()');
	}
	
	// ---------------------------------------------------------------------- 
	// barebones logger
	// ---------------------------------------------------------------------- 
	
	public static function logger($str, $level = E_USER_NOTICE) {
		if (Config::IS_LOGGING) {
			if ($level & Config::LOG_LEVEL) {  // arithmatic AND
	        	Utilities::flogger($str, $level);
	    	}
		}
	}
		
	// ---------------------------------------------------------------------- 
	// cobble together a full URL for the current script
	// ---------------------------------------------------------------------- 
	
	public static function current_page_url($query = true) {
		if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off')  { $pageURL = 'https'; }
		else { $pageURL = 'http'; } 
		$pageURL .= "://";
		if ($_SERVER["SERVER_PORT"] != "80") {
			$pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
	 	} else {
	  		$pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
	 	}
	 	
	 	if (! $query) {
	 		$u = explode('?', $pageURL);
	 		$pageURL = $u[0];
	 	}
	 	
	 	return $pageURL;
	}
	
	// ---------------------------------------------------------------------- 
	// reformat (many common) date strings into RSS/RFC822 format
	// ---------------------------------------------------------------------- 
	
	public static function munge_date($str) {
		$int_time = strtotime($str);
		return date(Config::RFC822_FORMAT, $int_time);
	}
	
	// ---------------------------------------------------------------------- 
	// assure Android and iOS language codes (2 digit lang + 2 digit locale)
	// are in a uniform format (i.e. "en" for lang-only, or "en_US" for both)
	// ---------------------------------------------------------------------- 
	
	public static function canonicalize_langcode($code) {
		if (! $code) { return null; }
		
		if (strstr($code, '-')) {
			$parts = explode('-', $code);
		} else if (strstr($code, '_')) {
			$parts = explode('_', $code);
		} else if (strstr($code, ' ')) {
			$parts = explode(' ', $code);
		} else {
			return strtolower($code);
		}
		
		return strtolower($parts[0]) . '_' . strtoupper($parts[1]);
	}
	
	// ---------------------------------------------------------------------- 
	// capture key error information from curl activities to the log
	// ---------------------------------------------------------------------- 

	public static function capture_curl_error($cinfo, $level) {
		$hr = new HttpResponse(false);
		$desc = $hr->retrieveDescriptions($cinfo['http_code']);
		if (! $desc) {
			$msg = 'invalid http code [' . $cinfo['http_code'] . '] returned from url [' . $cinfo['url'] . ']';
		} else {
			$msg  = 'HTTP returned [' . $cinfo['http_code'] . ' (' . $desc['s_desc'] . ')] from url [' . $cinfo['url'] . ']';
		}	
		if ($cinfo['redirect_url'] != null) {
			$msg .= ' redirect count [' . $cinfo['redirect_count'] . '] redirected to [' . $cinfo['redirect_url'] . ']';
		}
		if ($cinfo['size_upload'] != 0) {
			$msg .= ' upload size [' . $cinfo['size_upload'] . ']';
		}
		if ($cinfo['size_download'] != 0) {
			$msg .= ' download size [' . $cinfo['size_download'] . ']';
		}
		
		Utilities::logger($msg, $level);
	}

	// ----------------------------------------------------------------------
	// properly recompose a URL from components
	// from: http://php.net/manual/en/function.parse-url.php (with security mod)
	// ----------------------------------------------------------------------

	public static function recompose_url($parsed_url, $force = false) {
		if (! $force) {
			$scheme = isset($parsed_url['scheme']) ? $parsed_url['scheme'] . '://' : ''; 
		} else {
			$scheme = 'https://';
		} 
		$host     = isset($parsed_url['host']) ? $parsed_url['host'] : ''; 
		$port     = isset($parsed_url['port']) ? ':' . $parsed_url['port'] : ''; 
		$user     = isset($parsed_url['user']) ? $parsed_url['user'] : ''; 
		$pass     = isset($parsed_url['pass']) ? ':' . $parsed_url['pass']  : ''; 
		$pass     = ($user || $pass) ? "$pass@" : ''; 
		$path     = isset($parsed_url['path']) ? $parsed_url['path'] : ''; 
		$query    = isset($parsed_url['query']) ? '?' . $parsed_url['query'] : ''; 
		$fragment = isset($parsed_url['fragment']) ? '#' . $parsed_url['fragment'] : ''; 
		
  		return "$scheme$user$pass$host$port$path$query$fragment"; 
	}	
	
	// ----------------------------------------------------------------------
	// hash_algorithm_exists
	// ----------------------------------------------------------------------
	
	public static function hashAlgorithmExists($algo) {
		$algorithms = hash_algos();
		$i = count($algorithms) - 1;
		$test = strtolower($algo);
		do {
			if ($algorithms[$i] == $test) { return true; }
		} while(--$i >= 0);
		
		return false;
	}	
	
	// ---------------------------------------------------------------------- 
	// ---------------------------------------------------------------------- 
	// private methods
	// ---------------------------------------------------------------------- 
	// ---------------------------------------------------------------------- 
	
	// ---------------------------------------------------------------------- 
	// log to a (configured) file
	// ---------------------------------------------------------------------- 
	
	private static function flogger($str, $level) {
	
	   // account for script running as daemon, not in httpd
	        
	    $script = $_SERVER["SCRIPT_NAME"];
	    if (!$script) {
	    	$a = proc_get_status(getmypid());
	    	$script = $a['command'];
	    }
	        
	    // severity
	        
		switch ($level) {
	        case E_ERROR:
	        	$sev = 'ERROR';
	        	break;
			case E_WARNING:
				$sev = 'WARNING';
				break;
			case E_NOTICE:
				$sev = 'NOTICE';
				break;
			case E_USER_NOTICE:
				$sev = 'CHECKPOINT';
				break;
			default:
				$sev = 'NOTICE';
		}
			
	    // date, accurate to microseconds
	
	    $thedate = microtime(true);             // float!
	    $datesec = intval($thedate);
	    $dateu   = intval(($thedate * 1000000) - ($datesec * 1000000));
	
	    $dat = date("F j, Y H:i+s", $datesec);  // get and format seconds
	    $dat .= "." . substr($dateu, 0, 4);
	
	    $contents = $script . " [" . $dat . "]: " . $sev . ': ' . $str . "\n";
	
		$log = Config::logDirectory() . '/' . Config::LOGFILE;

		if (! file_exists($log)) {
			touch($log);
		} 
		
		error_log($contents, 3, $log);	
		
		// for CRON output, typically, dup message to stdout
		if (defined('CONSOLE_ECHO')) {
			print $contents . "\n";
		}       
	}	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}
?>
