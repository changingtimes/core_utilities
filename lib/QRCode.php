<?php
/**
 * ----------------------------------------------------------------------
 * component: QRCode
 * Generate a QRCode containing a text string or a URL with (optionally)
 * a custom URL scheme.
 * API documentation: http://goqr.me/api/doc/create-qr-code/
 * 
 * Usage:
 *	$phrase = "Hello World";
 * 
 *	$qr = new QRCode();	
 *	$qr->setFormat("jpg");
 *	$qr->setFile("qrcode"); // file extension set by file format
 *	$qr->setDirectory('.');
 *	$qr->setData($phrase);
 *	
 *	$qr->setCustomProtocol('scheme://...');  // to create URL
 *
 *	$qr->generateQRCode();
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\core_utilities;

use guardianproject\core_utilities\URL as URL;
use \Exception as Exception; 

define('OUTPUT_MAX_SIZE', 1000);
 
class QRCode {

	// ----------------------------------------------------------------------
	// private variables
	// ----------------------------------------------------------------------

	// constants
	private $service_endpoint = 'https://api.qrserver.com/';
	private $service_cmd      = 'v1/create-qr-code/';
	private $allowed_formats  = array('jpg','jpeg','png','svg','eps','gif');
	private $allowed_ecc      = array('L','M','Q','H');
	private $margin           = 1;
	
	// per session
	private $uh;
	
	private $output_file;
	private $output_directory;
	private $ecc;
	private $data;
	private $format;
	private $charset;
	private $custom_protocol;
	private $size;	// in form string NxN 
	

	// ----------------------------------------------------------------------
	// constructor
	// ----------------------------------------------------------------------	
	
	public function __construct() {
		$this->format  = 'jpg';		// sensible defaults
		$this->size    = '200x200';
		$this->ecc     = 'L';
		$this->charset = 'UTF-8';
		$this->output_file = 'qrcode';
		$this->output_directory = '.';
		$this->custom_protocol = '';
		
		$this->uh = new URL();
    }
    	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// generateQRCode
	// ----------------------------------------------------------------------

	public function generateQRCode() {
		try {
			$resp = $this->acquire();
		} catch (Exception $e) {
			Utilities::logger("QRCode error: " . $e->getMessage(), E_ERROR);
			return false;
		}

		return true;
	}

	// ----------------------------------------------------------------------
	// setData
	// ----------------------------------------------------------------------

	public function setData($data) {
		$this->data = $data;
	}

	// ----------------------------------------------------------------------
	// setCustomProtocol
	// use of this method will generate QRCode content in the form of a URL
	// with a custom URL "scheme".  Mobile operating systems know how to use
	// this custom scheme to fire the right handling application on the 
	// device.
	// ----------------------------------------------------------------------

	public function setCustomProtocol($p) {
		$this->custom_protocol = $p;
	}

	// ----------------------------------------------------------------------
	// setFile - name portion of file (file format is added automatically when used)
	// ----------------------------------------------------------------------

	public function setFile($name) {
		$this->output_file = $name;
	}

	// ----------------------------------------------------------------------
	// setDirectory - directory location to store file
	// ----------------------------------------------------------------------

	public function setDirectory($name) {
		$this->output_directory = $name;
	}
	
	// ----------------------------------------------------------------------
	// setSize - number of pixels per dimension
	// ----------------------------------------------------------------------

	public function setSizeInPixels($size) {
		if (($this->format != 'svg') && ($this->format != 'eps')) {
			if ($size > OUTPUT_MAX_SIZE) {
				throw new Exception('requested output size exceeds maximum of ' . OUTPUT_MAX_SIZE);
			}
		}
		$this->size = $size . 'x' . $size;
	}

	// ----------------------------------------------------------------------
	// setFormat
	// ----------------------------------------------------------------------

	public function setFormat($format) {
		$fmt = strtolower($format);
		if (! in_array($fmt, $this->allowed_formats)) {
			throw new Exception('type not supported: ' . $fmt);
		}
		$this->format = $fmt;
	}
	
	// ----------------------------------------------------------------------
	// setECC
	// ----------------------------------------------------------------------

	public function setECC($ecc) {
		if (! array_key_exists(strtoupper($ecc), $this->allowed_ecc)) {
			throw new Exception('ECC not supported: ' . strtoupper($ecc));
		}
		$this->ecc = strtoupper($ecc);
	}
	
	// ----------------------------------------------------------------------
	// setCharset
	// ----------------------------------------------------------------------

	public function setCharset($charset) {
		if (! array_key_exists(strtoupper($charset), $this->allowed_ecc)) {
			throw new Exception('ECC not supported: ' . strtoupper($charset));
		}
		$this->charset = strtoupper($charset);
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------	

	// ------------------------------------------------------------------
	// acquire - using curl() gives us more control over the retrieval 
	// process then file_get_contents(), e.g.
	// ------------------------------------------------------------------	
	
	private function acquire() {
		$url = $this->service_endpoint . $this->service_cmd;
		$url .= '?format=' . $this->format;
		$url .= '&size=' . $this->size;
		$url .= '&ecc=' . $this->ecc;
		$url .= '&charset=' . $this->charset;
		
		// instead of using the data as-is, fabricate a new url with
		// the data ITS data
		
		if ($this->custom_protocol) {
			$url .= '&data=' . urlencode($this->custom_protocol . urlencode($this->data));
		} else {
			$url .= '&data=' . urlencode($this->data);
		}

		$fn = $this->output_directory . '/' . $this->output_file . '.' . $this->format;
		$res = $this->uh->get($url, true, false, $fn);
		
		if ($res['http_code'] == '0') { 
			Utilities::logger("FAILED: TIMEOUT", E_ERROR);
			$res = false;
		} else if ($res['http_code'] != '200') {
			Utilities::logger("FAILED with HTTP CODE: [" . $info['http_code'] . "]", E_ERROR);
			$res = false;
		} 

		if ($res != true) {
			throw new Exception('QR request failed with HTTP response code [' . $info['http_code'] . ']');
		}
		return true;
	}
	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
?>