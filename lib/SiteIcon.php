<?php
/**
 * ----------------------------------------------------------------------
 * component: SiteIcon
 * Find, and manage a cache of, "favicon" type icons indexed by URL, stored 
 * in the local file system.  Note that the URL provided is automagically
 * "reduced" to the site's root URL to handle the request, since sites
 * are allowed only one favicon.  Thus, requests for "internal URL" return
 * the same as the root.
 *
 * Usage:
 * $si = new SiteIcon();
 * if ($si->iconExists($url) { $base64_icon = $si->getIcon($url); } // from cache
 * $base64_icon = $si->acquireIcon($url); // from cache, or from site (and save)
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\core_utilities;

use guardianproject\core_utilities\URL;
use guardianproject\core_utilities\Utilities;
use UtilitiesConfig as Config;

use \DOMDocument;

class SiteIcon {
	private $verbose_mode = false;
	private $uh;
	
	private $favicon_url;
	private $icon_dir;
	private $permitted_types = array(
		'image/vnd.microsoft.icon' => 'ico',
		'image/x-icon' => 'ico',
		'image/icon' => 'ico',
		'image/png' => 'png',
		'image/jpeg' => 'jpg',
		'image/gif '=> 'gif'
	);
	
	public function __construct($force = false) {
		$this->uh = new URL();
		$this->uh->setUserAgent('EthicalIconRetriever/1.0');
		$this->dir_name = 'icons';
		$this->icon_dir = Config::utilitiesDataDirectory() . '/' . $this->dir_name;

		if (! $this->initialize($force)) {
			throw new Exception('Object creation failed: ');
		}
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// setVerboseMode - for testing
	// ----------------------------------------------------------------------
	
	public function setVerboseMode($bool) {
		$this->verbose_mode = $bool;
	}
	
	// ----------------------------------------------------------------------
	// iconExists - locally only
	// ----------------------------------------------------------------------
	
	public function iconExists($url) {
		if (! $url) return false; 
		if ($this->retrieveIcon($url)) { return true; }
		return false;
	}

	// ----------------------------------------------------------------------
	// acquireIcon - either in cache, or from site (if latter, then store).
	// "direct" option means we know URL argument is verified (exists) and
	// is the site's root (which is used for the icon's storage key)
	// ----------------------------------------------------------------------
		
	public function acquireIcon($url, $direct = null) {
		if ($this->iconExists($url)) { return $this->getIcon($url); }
		
		// locate the icon if necessary
		if (! $direct) {
			$i_url = $this->find_icon_for($url);
			if (! $i_url) { return null; }
		} else {
			$i_url = $url;
		}

		// acquire the icon
		$res = $this->uh->acquire($i_url, true, false);
		if (! $res) { return null; }
		$arr = explode(';', $res['content_type']);
		$content_type = $arr[0];	
		if (! $content_type) {
			$content_type = $this->content_type_from_path($i_url);
			if (! $content_type) {
				if ($this->verbose_mode == true) { print "no content type in path [" . $i_url . "]; substituting PNG\n"; }
				$content_type = 'image/png';
			}
		}
		$extension = $this->file_extension_from_path($i_url);
			
		// get the proper content type 
		if (! $this->is_permitted_type($content_type)) {
			Utilities::logger("error in icon URL: " . $i_url . " with content type: " . $content_type, E_NOTICE);
			return null;
		}		

		// store the icon SOMETHING IS NOT RIGHT HERE
		
		if ($direct) { $key_url = $direct; }
		else { $key_url = $url; }
		if (! $this->saveIcon($key_url, $extension, $res['body'])) { return null; }
		
		return $this->asBase64($res['body'], $extension); 
	}
	
	// ----------------------------------------------------------------------
	// captureIcon - retrieve icon bits from site (no storage)
	// ----------------------------------------------------------------------
	
	public function captureIcon($url) {
		$i_url = $this->find_icon_for($url);
		if (! $i_url) { return null; }

		// acquire the icon
		$res = $this->uh->acquire($i_url, true, false);
		if (! $res) { return null; }
		$content_type = $res['content_type'];
		if (! $content_type) {
			$content_type = $this->content_type_from_path($i_url);
			if (! $content_type) {
				if ($this->verbose_mode == true) { print "no content type in path [" . $i_url . "]; substituting PNG\n"; }
				$content_type = 'image/png';
			}
		}	
		
		// get the proper content type 
		if (! $this->is_permitted_type($content_type)) {
			Utilities::logger("error in icon URL: " . $i_url . " with content type: " . $content_type, E_NOTICE);
			return null;
		}		

		return $this->asBase64($res['body'], $extension); 
	}

	// ----------------------------------------------------------------------
	// getIcon/retrieveIcon - local retrieval only
	// ----------------------------------------------------------------------
	
	public function getIcon($url) { return $this->retrieveIcon($url); }
	
	public function retrieveIcon($url) {
		if (! $url) return null;
		
		$root = $this->site_root($url, true);
		$key = $this->generate_key($root);
		$dir = substr($key,0,1);
		if (! file_exists($this->icon_dir . '/' . $dir)) {
			Utilities::logger('icon sub-directory [' . $this->icon_dir . '/' . $dir . '] does not exist', E_ERROR);
			return null;
		}
		list($actual_path, $file_extension) = $this->icon_file_exists($this->icon_dir . '/' . $dir, $key);
		if (! $actual_path) {
			Utilities::logger('icon file base [ICON_DIR/' . $dir . '/' . $key. '] does not exist', E_USER_NOTICE);
			return null;
		}

		$bits = file_get_contents($actual_path);
		return $this->asBase64($bits, $file_extension);		
	}
	
	// ----------------------------------------------------------------------
	// asBase64 - encode icon bits
	// format: <img alt="Embedded Image" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgA..." />
	// ----------------------------------------------------------------------	

	public function asBase64($icon, $ext) {
		$asStr = "data:image/" . $ext . ";base64,";
		return $asStr . base64_encode($icon);
	}
	
	// ----------------------------------------------------------------------
	// saveIcon - store an icon in the file system
	// ----------------------------------------------------------------------

	public function saveIcon($url, $ext, $icon_bits) {
		if (! $url) return false;
		if (strlen($icon_bits) == 0) {
			Utilities::logger('empty string provided for icon contents for URL [' . $url . ']', E_WARNING);
			return false;
		}
		
		// assure we use valid site root to generate key 
		$root = $this->site_root($url, true);
		$key = $this->generate_key($root);
		$dir = substr($key,0,1);

		if (! file_exists($this->icon_dir . '/' . $dir)) {
			Utilities::logger('icon sub-directory [' . $this->icon_dir . '/' . $dir . '] does not exist', E_ERROR);
			return false;
		}		

		$fn = $this->icon_dir . '/' . $dir . '/' . $key . '.' . $ext;
		if (! file_put_contents($fn, $icon_bits)) {
			Utilities::logger('error writing data to file [' . $fn . ']', E_ERROR);
			return false;
		}
		
		Utilities::logger('cached icon for site root ' . $root . ' in ' . $fn, E_NOTICE);
		return true;
	}

	// ----------------------------------------------------------------------
	// findIconUrl - given a URL, find that site's icon URL
	// ----------------------------------------------------------------------
	
	public function findIconUrl($url) {
		if (! $url) return false; 

		$i_url = $this->find_icon_for($url);
		if (! $i_url) { 
			Utilities::logger("unable to find icon for URL [$url]", E_WARNING); 
			return false;
		}
			
		$out_url = $this->verify_icon_location($i_url);
		$result = $this->uh->ping($out_url, false, false);
		
		if (array_key_exists('redirect_url', $result) &&
			($result['redirect_url'] != '')) { $out = $result['redirect_url']; }
		
		$out = $result['url'];
		$this->favicon_url = $out;
		if ($this->verbose_mode == true) { print "verified icon URL: " . $out . "\n"; }
		
		return $out;
	}

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// locate a site's favicon
	// ----------------------------------------------------------------------
		
	private function find_icon_for($url) {
		$out_url = null;
		
		// Find the "root" URL
		$root = $this->site_root($url, true);
		if (! $root) { return null; }		

		// Get the site's root page and see if it offers a favicon
		// via a <link rel="icon"> type reference
		// (most "modern" and reliable method of finding the icon)
		$res = $this->uh->acquire($root, false);
		$i_url = $this->locate_icon_link($res['body']);
		if ($i_url) {
			// check for a relative path returned
			if (substr($i_url, 0, 3) != 'htt') {
				if (substr($i_url, 0, 2) == '//') {	// odd missing-scheme format
					$parts = parse_url($i_url);
					$out_url = trim($root, '/') . trim($parts['path'], '/'); 
				} else { // normal relative path
					$out_url = trim($root, '/') . '/' . trim($i_url, '/');
				}
			} else {
				$out_url = $i_url;
			}

			// one more verification!
			$out_url = $this->verify_icon_location($out_url);			
			if ($out_url) {
				Utilities::logger("url located via root document: " . $out_url . " (verified)", E_USER_NOTICE);
				if ($this->verbose_mode == true) { print "icon url located via root document: " . $out_url . " (verified)\n"; } 
			}
		}
		
		if ($out_url) { return $out_url; }
		
		// If not available, check the canonical (old-school) location(s)
		// (fraught with problems as returned URL might be an error page
		// or other content page)
		$newu =  $root . 'favicon.';
		$i_types = array('ico','png','jpg');

		foreach ($i_types as $type) {
			$nt = $newu . $type;
			$out_url = $this->verify_icon_location($nt);
			if ($out_url) {
				Utilities::logger("old-school favicon location: [" . $out_url . "]", E_USER_NOTICE); 
				break;
			}
		}
		if (! $out_url) {
			Utilities::logger('can not locate favicon for site ' . $url, E_NOTICE);
		}
		if ($this->verbose_mode == true) { print "icon url located via old-school rules: " . $out_url . " (verified)\n"; }
		return $out_url;
	}

	// ----------------------------------------------------------------------
	// locate_icon_link - find the <link rel=icon> (or similar) in the 
	// supplied HTML text
	// ----------------------------------------------------------------------
	
	private function locate_icon_link($html_page) {
		$dom = new DOMDocument;
		@$dom->loadHTML($html_page);

		$i_url = null;
		$links = $dom->getElementsByTagName('link');

		$n = 0;
		$got_shortcut = false;
		foreach ($links as $link) {
    		$rel = strtolower($link->getAttribute('rel'));
    		// prefer this link, since it usually points to the smallest icon
    		if (($rel == 'shortcut icon') || ($rel == 'icon')) {
    			$i_url = $link->getAttribute('href');
    			$got_shortcut = true;
    		}
    		if (($rel == 'apple-touch-icon') ||
    			($rel == 'apple-touch-icon-precomposed')) {
    			if ($got_shortcut == false) {
					$i_url = $link->getAttribute('href');
					break;
    			}
    		}
    		$n++;  
		}

		if (! $i_url) {
			Utilities::logger('HTML page (' . $n . ' links) does not define an icon URL', E_USER_NOTICE);
		} else {
			if ($this->verbose_mode == true) { print "icon located in document at: " . $i_url . "\n"; }
		}
		
		return $i_url;
	}

	// ----------------------------------------------------------------------
	// site_root - determine a site's root directory
	// ----------------------------------------------------------------------

	private function site_root($url, $force = false) {
		if (! $url) { return null; }
		
		$parts = parse_url($url);
		$root = $parts['scheme'] . '://' . $parts['host'];
		if (array_key_exists('port', $parts)) { $root .= ':' . $parts['port']; }
		$root .= '/';
		
		// if necessary, make sure this URL exists
		// or capture its redirect
		$test = $root;
		if ($force) {
			$root = $this->verify_root_location($test);
			if ($root != $test) {
				Utilities::logger('verification changed root from [' . $test . '] to [' . $root . ']', E_USER_NOTICE);
			}
		}
		if ($this->verbose_mode == true) { print "site root: " . $root . "\n"; }
		return $root;
	}
	
	// ----------------------------------------------------------------------
	// is_permitted_type
	// ----------------------------------------------------------------------
	
	private function is_permitted_type($content_type) {
		foreach ($this->permitted_types as $c_type => $ext) {
			if ($content_type == $c_type) { return true; }
		}
		if ($this->verbose_mode == true) { print "odd icon content type: $content_type\n"; }
		return false;
	}

	// ----------------------------------------------------------------------
	// icon_file_exists - does the icon file already exist?
	// ----------------------------------------------------------------------

	private function icon_file_exists($dir, $name) {
		foreach ($this->permitted_types as $c_type => $ext) {
			$attempt = $dir . '/' . $name . '.' . $ext;
			if (file_exists($attempt)) { return array($attempt, $ext); }
		}
		
		return array(null, null);
	}
	
	// ----------------------------------------------------------------------
	// generate_key - key to both file name and directory
	// ----------------------------------------------------------------------
	
	private function generate_key($input) {
		$the_hash = hash('md5', $input);
		if ($this->verbose_mode == true) { print "key for site root is [" . $the_hash . "]\n"; }
		return $the_hash;
	}
		
 	// ----------------------------------------------------------------------
	//  verify_icon_location
	// ----------------------------------------------------------------------
	
	private function verify_icon_location($url) {
		$out = $this->uh->ping($url, true, false); // follow redirect
		if ($out['url'] != $url) {
			$out = $this->uh->ping($out['url'], false, false);
		} 

		if (! $out) { return null; }
		
	    if (array_key_exists('content_type', $out) && 
	    	! $this->is_permitted_type($out['content_type'])) { 
			// check against file extension
			$type = $this->content_type_from_path($out['url']);
			if (! $this->is_permitted_type($type)) {
		    	return null;
		    }
		}
		
		return $out['url'];
	}
	
	// ----------------------------------------------------------------------
	//  verify_root_location
	// ----------------------------------------------------------------------
	
	private function verify_root_location($url) {
		$out = $this->uh->ping($url, true, false);
		if (! $out) { return null; }
		if ($out['url'] != $url) { $out = $this->uh->ping($out['url'], false, false); }
		else if (array_key_exists('effective_url', $out)) { 
			$out = $this->uh->ping($out['effective_url'], false, false);
		}
		
		return $out['url'];
	}	

	// ----------------------------------------------------------------------
	// file_extension_from_path - force content-type from file extension (ugh!)
	// ----------------------------------------------------------------------
		
	private function file_extension_from_path($url) {
	    $parts = parse_url($url);
	    $pieces = explode('/', strrev($parts['path']));
	    $filen = strrev($pieces[0]);
	    if (! $filen) { return null; }
	    list ($name, $extension) = explode('.', $filen);
	    
	    return $extension;
	}
	
	// ----------------------------------------------------------------------
	// content_type_from_path - force content-type from file extension (ugh!)
	// ----------------------------------------------------------------------
		
	private function content_type_from_path($url) {
		$extension = $this->file_extension_from_path($url);
		if (! $extension) { return null; }

		foreach ($this->permitted_types as $type => $ext) {
	    	if ($ext == $extension) { return $type; }
	    }
	    
	    return null;	
	}
	
	// ----------------------------------------------------------------------
	// initialize - check that we have our icon directory hierarchy beneath us
	// ----------------------------------------------------------------------
	
	private function initialize() {
		$where = $this->icon_dir;
		if (file_exists($where)) { return true; } // already created
		
		// create necessary directory hierarchy
		if (! mkdir($where)) {
			Utilities::logger('unable to create base icon directory [' . $where . ']', E_ERROR);
			return false;
		}
		$n = array('a','b','c','d','e','f','0','1','2','3','4','5','6','7','8','9');
		foreach ($n as $m) {
			$sub = $where . '/' . $m;
			if (! mkdir($sub)) {
				Utilities::logger('unable to create icon sub-directory [' . $sub . ']', E_ERROR);
				return false;
			}
		}
		
		Utilities::logger('created directory hierarchy to store site icons',E_NOTICE);
		return true;
	}
	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
?>