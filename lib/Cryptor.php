<?php
/** 
 * ----------------------------------------------------------------------
 * component: Cryptor
 * encryption and decryption of content via private key with openssl.
 * (adapted from: http://blog.turret.io/the-missing-php-aes-encryption-example/)
 *
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
**/

namespace guardianproject\core_utilities;

use UtilitiesConfig as Config; 
use guardianproject\core_utilities\UUID as UUID;
use \Exception;

class Cryptor {
	// ----------------------------------------------------------------------
	// private variables
	// ----------------------------------------------------------------------

	private $encryption_key;
	private $encryption_algorithm;
	private $init_vector;
	private $iv_is_set;
	private $chunked;
	
	// ----------------------------------------------------------------------
	// constructor
	// ----------------------------------------------------------------------	
	
	public function __construct($chunked = false) {
		$this->encryption_algorithm = Config::ENC_ALGORITHM;

		if (! file_exists(Config::encryptionKeyFile())) {
			throw new Exception('encryption keyfile ' . Config::encryptionKeyFile() . ' not found.');
		}
		
		$this->encryption_key = trim(file_get_contents(Config::encryptionKeyFile()));
		$this->chunked = $chunked;
		$this->iv_is_set = false;
    }

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public static methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
    
    public static function generateKey($seed_string = null) {
    	if (! $seed_string) {
    		$seed_string = UUID::v4();
    	}
    	
    	// Generate a 256-bit encryption key
		$key = bin2hex(hash('sha256', $seed_string, true));
		
		// This is stored somewhere instead of recreating it each time
		file_put_contents(Config::encryptionKeyFile(), $key);
		chmod(Config::encryptionKeyFile(),0600);  // read/write owner only
		
		return true;
    }
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// get/setInitializationVector - use an external IV instead of creating one
	// ----------------------------------------------------------------------

	public function getInitializationVector() { return $this->init_vector; }
	
	public function setInitializationVector($iv) {
		$this->init_vector = $iv;	// base64 encoded!!!
		if (! $this->init_vector) { $this->iv_is_set = false; }
		else { $this->iv_is_set = true; }
	}
	
	// ----------------------------------------------------------------------
	// setChunkMode
	// ----------------------------------------------------------------------
		
	public function setChunkMode($boolean) { $this->chunked = $boolean; }

	// ----------------------------------------------------------------------
	// encrypt
	// ----------------------------------------------------------------------
		
	public function encrypt($data) {
		return $this->package($this->encrypt_data($data));
	}

	// ----------------------------------------------------------------------
	// decrypt
	// ----------------------------------------------------------------------
	
	public function decrypt($blob) {
		return $this->decrypt_data($blob);
	}

	// ----------------------------------------------------------------------
	// encryptFile - generates temporary file (unlink when you're done!)
	// ----------------------------------------------------------------------
		
	public function encryptFile($fn) {
		$data = file_get_contents($fn);
		$output = $this->encrypt($data);

		$tfn = tempnam(sys_get_temp_dir(), substr(sha1(time()), 0, 4));
		$fh = fopen($tfn, "w");
		fwrite($fh, $output);
		fclose($fh);

		return $tfn;
	}

	// ----------------------------------------------------------------------
	// encryptToFile
	// ----------------------------------------------------------------------
		
	public function encryptToFile($fn, $data) {
		$output = $this->encrypt($data);
		file_put_contents($fn, $output);
	}

	// ----------------------------------------------------------------------
	// decryptFile
	// ----------------------------------------------------------------------
	
	public function decryptFile($fn) {
		$blob = file_get_contents($fn);
		return $this->decrypt($blob);
	}

	// ----------------------------------------------------------------------
	// decryptToFile
	// ----------------------------------------------------------------------
	
	public function decryptToFile($fn, $blob) {
		$output = $this->decrypt($blob);
		file_put_contents($fn, $output);
	}
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// create_initialization_vector
	// ----------------------------------------------------------------------
	
	private function create_initialization_vector() {
		// Generate an initialization vector
		// This *MUST* be available for decryption as well
		$this->init_vector = base64_encode(openssl_random_pseudo_bytes(openssl_cipher_iv_length($this->encryption_algorithm)));	
	}

	// ----------------------------------------------------------------------
	// encrypt_data
	// ----------------------------------------------------------------------
		
	private function encrypt_data($data) {
		if (! $this->iv_is_set) {
			//reset our init vector
			$this->create_initialization_vector();
		}
		// Encrypt $data using aes-256-cbc cipher with the given encryption key and 
		// our initialization vector. The 0 gives us the default options, but can
		// be changed to OPENSSL_RAW_DATA or OPENSSL_ZERO_PADDING
		$encrypted = openssl_encrypt($data, $this->encryption_algorithm, hex2bin($this->encryption_key), 0, base64_decode($this->init_vector));
		return $encrypted; // Base64Encoded automatically
	}

	// ----------------------------------------------------------------------
	// decrypt_data
	// ----------------------------------------------------------------------
	
	private function decrypt_data($blob) {
		list($data, $iv) = $this->depackage($blob);
		if ($this->iv_is_set) { $the_iv = $this->init_vector; }
		else { $the_iv = $iv; }
		$decrypted = openssl_decrypt($data, $this->encryption_algorithm, hex2bin($this->encryption_key), 0, base64_decode($the_iv));
		
		return $decrypted;
	}
	
	// ----------------------------------------------------------------------
	// package
	// ----------------------------------------------------------------------
	
	private function package($encrypted) {
		// If we lose the $iv variable, we can't decrypt this, so append it to the 
		// encrypted data with a separator that we know won't exist in base64-encoded 
		// data
		if (strlen($this->init_vector) < 20) {
			$encrypted = $encrypted . ':' . base64_encode($this->init_vector);
		} else {
			$encrypted = $encrypted . ':' . $this->init_vector;
		}
		if ($this->chunked) {
			$chunked = $this->enchunk($encrypted);
			return $chunked;
		}
		return $encrypted;
	}
	
	// ----------------------------------------------------------------------
	// depackage
	// ----------------------------------------------------------------------
	
	private function depackage($blob) {
		if ($this->chunked) { $data = $this->dechunk($blob); }
		else { $data = $blob; }
		
		// To decrypt, separate the encrypted data from the initialization vector ($iv)
		return explode(':', $data);
	}

	// ----------------------------------------------------------------------
	// enchunk
	// ----------------------------------------------------------------------

	private function enchunk($encoded_data) {
		$len = strlen($encoded_data);

		$amt = 76;	
		if ($len <= $amt) { return $encoded_data; }
	
		$chunks = '';
		$ptr = 0;
		do {
			$chunks .= substr($encoded_data, $ptr, $amt) . "\n";
			$ptr += $amt;
		} while ($ptr < $len);
	
		return $chunks;
	}

	// ----------------------------------------------------------------------
	// dechunk
	// ----------------------------------------------------------------------

	private function dechunk($chunked_data) {
		$dechunked = str_replace(array("\n","\r"), '', $chunked_data);
		return $dechunked;
	}
	
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/
}

?>
