<?php
/**
 * ----------------------------------------------------------------------
 * component: CountryFlag 
 * Flag images for all ISO 3166 countries, retreived using ISO 3166 Country 
 * Code or (English) Country Name. Flags available in 5 resolutions. 
 * Icon-sized flags -- 40x20px -- are stored locally in base64.
 * All other flag sizes retrieved from CDN and delivered in base64.
 * Emoji are in HTML-friendly format.
 *
 * See http://flagpedia.net/index
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/

namespace guardianproject\core_utilities;

use \PDO as PDO;
use \PDOException as PDOException;
use guardianproject\core_utilities\Utilities;
use guardianproject\core_utilities\CountryCode as CountryCode;
use guardianproject\core_utilities\URL as URL;
use UtilitiesConfig as Config;

class CountryFlag extends DataStore {		
	private $flags_url_base   = 'http://flagcdn.com/';
	private $un_flag_location = 'https://flagdownload.com/wp-content/uploads/Flag_of_United_Nations-2048x1365.png';
	private $tmpfile          = './data/a3b6c2f1.png';
	
	private $sizes       = array(
		'icon'   => 40,
		'mini'   => 160,
		'normal' => 640,
		'big'    => 1280,
		'ultra'  => 2560
	);
	private $file_type   = 'png';
	
	private $uh;
	private $cc;
		
	public function __construct($force = false) {

		$this->builds_from_source = true;

		$this->info_database = 'country_info.db';
		$this->static_data   = 'flags2.csv';
		$this->table_name    = 'flags';
		
		$this->cc = new CountryCode();
		$this->uh = new URL();
		
		$this->db_loc   = Config::utilitiesDbDirectory() . '/' . $this->info_database;
		$this->data_loc = Config::utilitiesDataDirectory() . '/' . $this->static_data;
		
		if (! $this->initialize($force)) {
			throw new Exception('Object creation failed. Database ' . $this->info_database . ' corrupt or incomplete');
		}
	}	

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// getFlagForCountryCode
	// ----------------------------------------------------------------------
		
	public function getFlagForCountryCode($code, $size = 'icon') {
		$cc = strtoupper($code);
		if (! $this->cc->CountryCodeExists($cc)) {
			return null;
		}
		
		// special for UN flag
		if ($cc == 'UN') {
			return $this->handle_un_flag($size);
		}
		
		// check size
		if (! array_key_exists($size, $this->sizes)) {
			Utilities::logger('wrong flag size requested [' . $size . ']. Use one of (' . implode('|', array_keys($this->sizes)) . ')', E_WARNING);
			return null;
		}
		
		if ($size == 'icon') {
			return $this->retrieve_flag_icon($cc);
		}

		$url = $this->compose_url($cc, $size);
		return $this->retrieve_flag_data($url);
	}	

	// ----------------------------------------------------------------------
	// getEmojiForCountryCode
	// ----------------------------------------------------------------------
		
	public function getEmojiForCountryCode($code) {
		$cc = strtoupper($code);
		if (! $this->cc->CountryCodeExists($cc)) {
			return null;
		}

		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE iso3166_cc2=?';
		Utilities::logger("PDO query: " . $sql . " (with $code)", E_USER_NOTICE);
	    
	    try {
	    	$q = $this->db->prepare($sql);
			$q->execute(array($code));
	   		$res = $q->fetch(PDO::FETCH_ASSOC);
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving flag icon for [' . $cc2 . ']', E_USER_NOTICE);
			return null;
		}		
	   	
	   	if (! $res) { return null; }
	   	return $res['emoji'];
	}	
	
	// ----------------------------------------------------------------------
	// getFlagForCountryName
	// ----------------------------------------------------------------------
		
	public function getFlagForCountryName($name, $size = 'icon') {
		$code = $this->cc->retrieveCountryCode($name);
		if ($code) {
			return $this->getFlagForCountryCode($code, $size); 
		}
		return null;
	}

	// ----------------------------------------------------------------------
	// rebuild - specialized to rebuild this object from source
	// ----------------------------------------------------------------------
	
	public function rebuild() {
		if (! $this->unload_table()) { return false; }
		if (! $this->acquire_flags()) { return false; }
		
		Utilities::logger('country flag database rebuilt from network data source', E_NOTICE);
		return true;
	}

	// ----------------------------------------------------------------------
	// updateFlagIcon
	// ----------------------------------------------------------------------
	
	public function updateFlagIcon($cc) {
		if (strtoupper($cc) == 'UN') {
			$data = $this->handle_un_flag('icon');
		} else {
			$url = $this->flags_url_base . '/w40/' . strtolower($cc) . '.png';
			$data = $this->retrieve_flag_data($url);
		}
		
		print "updating icon for [$cc]: size " . strlen($data) . "\n";
		return $this->update_flag_icon($cc, $data);
	}

	// ----------------------------------------------------------------------
	// dump
	// ----------------------------------------------------------------------

	public function dump($to_file = null) {
		if ($to_file == null) {
			$ran = substr(sha1(time()), 0, 12);
			$to_file = tempnam(sys_get_temp_dir(), 'country_flag_backup_' . $ran);
		}
		$fp = fopen($to_file, "w");
		if (! $fp) {
			Utilities::logger("dump: could not open " . $to_file . " for writing", E_WARNING);
			return false;
		}	

		fputcsv($fp, array('iso3166_cc2','emoji_code','b64icon'));	// write CSV header
		
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    	while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    		fputcsv($fp, array($res['iso3166_cc2'], $res['emoji'], $res['flag']));
	    	}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error dumping flag data (' . $e->getMessage() . ')', E_ERROR);
			return false;
		}		

		fclose($fp);
		Utilities::logger("contents of " . $this->table_name . " dumped to " . $to_file, E_NOTICE);
		return true;
	}
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private functions
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// acquire_flags
	// ----------------------------------------------------------------------

	private function acquire_flags() {
		try {
			$this->get_flag_icons();
		} catch (Exception $e) {
			return false;
		}
		return true;
	}
		
	// ----------------------------------------------------------------------
	// get_flag_icons
	// ----------------------------------------------------------------------
	
	private function get_flag_icons() {
		$ccs = $this->cc->allCountryCodes();
		foreach ($ccs as $cc) {
			$url = $this->compose_url($cc, 'icon');
			try {
				$b64data = $this->retrieve_flag_data($url);
				$this->load_entry(strtoupper($cc), $b64data);
				print "loaded flag icon for [$cc]\n";
				usleep(mt_rand(100000,200000));
			} catch (Exception $e) {
				Utilities::logger("no flag data for country code [" . strtoupper($cc) . "] at [" . $url . "]", E_WARNING);
			}
		}

		return true;
	}

	// ----------------------------------------------------------------------
	// retrieve_flag_data
	// ----------------------------------------------------------------------
		
	private function retrieve_flag_data($url) {
		if (! $url) {
			Utilities::logger('retrieve_flag_data: no URL supplied', E_WARNING);
			return null;		
		}
 
  		$this->uh->get($url, true, false, $this->tmpfile); 
  		if (! file_exists($this->tmpfile)) {       
			Utilities::logger("failed to load " . $url . " [" . $res['http_code'] . "]", E_WARNING);
			return null;
		} 
		
		return base64_encode(file_get_contents($this->tmpfile));
	}
	
	// ----------------------------------------------------------------------
	// compose_url
	// ----------------------------------------------------------------------
	
	private function compose_url($cc, $size) {
		// check size
		if (! array_key_exists($size, $this->sizes)) {
			Utilities::logger('wrong flag size requested [' . $size . ']. Use one of (' . implode('|', array_keys($this->sizes)) . ')', E_WARNING);
			return null;
		}

		// size the output
		switch ($size) {
			case 'icon':
				$path = 'w' . $this->sizes['icon'];
				break;
			case 'mini':
				$path = 'w' . $this->sizes['mini'];
				break;
			case 'normal':
				$path = 'w' . $this->sizes['normal'];
				break;
			case 'big':
				$path = 'w' . $this->sizes['big'];
				break;
			case 'ultra':
				$path = 'w' . $this->sizes['ultra'];
				break;
		}
		
		return $this->flags_url_base . $path . '/' . strtolower($cc) . '.png';
	}
	
	// ----------------------------------------------------------------------
	// resize_flag_data
	// ----------------------------------------------------------------------
	
	private function resize_flag_data($source, $size) {
		// check size
		if (! array_key_exists($size, $this->sizes)) {
			Utilities::logger('wrong flag size requested [' . $size . ']. Use one of (' . implode('|', array_keys($this->sizes)) . ')', E_WARNING);
			return null;
		}
		
		$input = imagecreatefrompng($source);
		if (! $input) {
			Utilities::logger('resize_flag_data: input is not a byte string', E_ERROR);
			return null;
		}
		
		// size the output
		switch ($size) {
			case 'icon':
				$out_height = $this->sizes['icon'];
				break;
			case 'mini':
				$out_height = $this->sizes['mini'];
				break;
			case 'normal':
				$out_height = $this->sizes['normal'];
				break;
			case 'big':
				$out_height = $this->sizes['big'];
				break;
			case 'ultra':
				$out_height = $this->sizes['ultra'];
				break;
		}

		// calculate dimensions
		$dim = getimagesize($source);
		$in_width = $dim[0];
		$in_height = $dim[1];
		$ratio = $in_width / $in_height;
		$out_width = intval($out_height * $ratio);
		
		// build output in memory	
		$output = imagecreatetruecolor($out_width, $out_height);
		imagecopyresized($output, $input, 0, 0, 0, 0, $out_width, $out_height, $in_width, $in_height);

		// write to, read from temporary storage
		$tf = '/tmp/gd_' . uniqid() . '.png';
		$mp = fopen($tf, 'w');
		imagepng($output, $mp); // automatically closes resources
		$mp = fopen($tf, 'r');
		$bits = fread($mp, 100000);
		fclose($mp);
		
		unlink($tf);
		imagedestroy($output);
		return base64_encode($bits);			
	}
	
	// ----------------------------------------------------------------------
	// update_flag_icon
	// ----------------------------------------------------------------------
		
	private function update_flag_icon($cc, $data) {
		$sql = 'UPDATE ' . $this->table_name . ' SET flag=\'' . $data . '\' WHERE iso3166_cc2=\'' . strtoupper($cc) . '\';';
	    Utilities::logger("PDO stmt: " . $sql, E_USER_NOTICE);
	    
	    try {
			$q = $this->db->prepare($sql);
			$q->execute();
		} catch (PDOException $e) {
			Utilities::logger('PDO: update_flag_icon failed to load icon for ' . $cc . ' (' . $e->getMessage() . ')', E_ERROR);      
			return false;
		}
		
		return true;
	}

	// ----------------------------------------------------------------------
	// handle_un_flag - special flag, UN not an official part of ISO3166, so
	// flag comes from different location
	// ----------------------------------------------------------------------
			
	private function handle_un_flag($size) {
		if (! $this->retrieve_flag_data($this->un_flag_location)) {
			print 'WARNING: UN Flag no longer accessible at ' . $this->un_flag_location . "\n";
			Utilities::logger('UN Flag no longer accessible at ' . $this->un_flag_location, E_ERROR);      
			return null;
		} else {
			$data = $this->resize_flag_data($this->tmpfile, $size);
			unlink($this->tmpfile);
		}
		
		return $data;
	}	

	// ----------------------------------------------------------------------
	// retrieve_flag_icon
	// ----------------------------------------------------------------------
	
	private function retrieve_flag_icon($code) {		
		$cc2 = strtoupper($code);
		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE iso3166_cc2=?';
		Utilities::logger("PDO query: " . $sql . " (with $cc2)", E_USER_NOTICE);
	    
	    try {
	    	$q = $this->db->prepare($sql);
			$q->execute(array($cc2));
	   		$res = $q->fetch(PDO::FETCH_ASSOC);
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving flag icon for [' . $cc2 . ']', E_USER_NOTICE);
			return null;
		}		
	   	
	   	if (! $res) { return null; }
	   	return $res['flag'];
	}
			
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// protected functions
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
				
	// ----------------------------------------------------------------------
	// load_table
	// ----------------------------------------------------------------------

	protected function load_table() {
		$fcc = fopen($this->data_loc, 'r');
		if (! $fcc) {
			Utilities::logger("error opening static flag data file " . $this->data_loc, E_ERROR);
			return false;
		}
		if (! $this->unload_table()) {
			return false;
		}
				
		$num = 0;
		$vals = fgetcsv($fcc, 200, ','); // strip header
		while ($vals = fgetcsv($fcc, 50000, ',')) {
			$this->load_entry($vals[0], $vals[1], $vals[2]);
			$num++;
		}
		
		Utilities::logger($num . ' entries added to flag table', E_NOTICE);
		fclose($fcc);
		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// load_entry
	// ----------------------------------------------------------------------

	protected function load_entry($code, $emoji, $flag_data) {		
	    $sql = 'INSERT OR REPLACE INTO ' . $this->table_name . ' VALUES (?,?,?);';
	    Utilities::logger("PDO stmt: " . $sql, E_USER_NOTICE);
	    
	    try {
			$q = $this->db->prepare($sql);
			$q->execute(array(strtoupper($code), $emoji, $flag_data));
		} catch (PDOException $e) {
			Utilities::logger('PDO: load_entry failed to insert values (' . $e->getMessage() . ')', E_ERROR);      
			return false;
		}
		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// create_table
	// ----------------------------------------------------------------------

	protected function create_table() {
		$sql = 'CREATE TABLE ' . $this->table_name . ' (iso3166_cc2 CHARACTER(2) PRIMARY KEY, emoji TEXT, flag TEXT);';
		Utilities::logger("PDO stmt: " . $sql, E_NOTICE);
		
		try {
			$this->db->exec($sql);
		} catch (PDOException $e) {
	    	Utilities::logger("PDO error creating database table in " . $this->db_loc . ' is ' . $e->getMessage(), E_ERROR);
	    	return false;
	    }
	    
	    return true;
	}
		
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
?>