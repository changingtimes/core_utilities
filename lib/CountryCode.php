<?php
/**
 * ----------------------------------------------------------------------
 * component: CountryCode (ISO 3166 Country Coding and auxiliary Information) 
 * See https://en.wikipedia.org/wiki/ISO_3166-1
 * 
 * ----------------------------------------------------------------------
 * @author David Oliver <david@guardianproject.info>
 * @license http://www.gnu.org/copyleft/lesser.html GNU Lesser General Public License
 * ----------------------------------------------------------------------
 **/
 
namespace guardianproject\core_utilities;

use \PDO as PDO;
use \PDOException as PDOException;
use \Exception;

use guardianproject\core_utilities\DataStore;
use guardianproject\core_utilities\Utilities;
use UtilitiesConfig as Config;

class CountryCode extends DataStore {	
	private $aux_list = 'iso3166_aux.csv';
	private $aux_loc;
	
	// for states that are not independent, define the association
	private $territories = array(
		'AI' => 'GB',
		'AQ' => 'UN',		// this is only partly valid
		'AS' => 'US',
		'AW' => 'NL',
		'AX' => 'FI',
		'BL' => 'FR',
		'BM' => 'GB',
		'BQ' => 'NL',
		'BV' => 'NP',
		'CC' => 'AU',
		'CK' => 'NZ',
		'CW' => 'NL',
		'CX' => 'AU',
		'EH' => 'MA',
		'FK' => 'GB',
		'FO' => 'DK',
		'GF' => 'FR',
		'GG' => 'GB',
		'GI' => 'GB',
		'GL' => 'DK',
		'GP' => 'FR',
		'GS' => 'GB',
		'GU' => 'US',
		'HK' => 'CN',
		'HM' => 'AU',
		'IM' => 'GB',
		'IO' => 'GB',
		'JE' => 'GB',
		'KY' => 'GB',
		'MF' => 'FR',
		'MO' => 'CN',
		'MP' => 'US',
		'MQ' => 'FR',
		'MS' => 'GB',
		'NC' => 'FR',
		'NF' => 'AU',
		'NU' => 'NZ',
		'PF' => 'FR',
		'PM' => 'FR',
		'PN' => 'GB',
		'PR' => 'US',
		'PS' => 'UN',		// invalid, but avoids some politics
		'RE' => 'FR',
		'SH' => 'GB',
		'SJ' => 'NO',
		'SX' => 'NL',
		'TC' => 'GB',
		'TF' => 'FR',
		'TK' => 'NZ',
		'TW' => 'CN',
		'UM' => 'US',
		'VG' => 'GB',
		'VI' => 'US',
		'WF' => 'FR',
		'YT' => 'FR'
	);
	
	public function __construct($force = false) {
		$this->builds_from_source = false;

		$this->info_database = 'country_info.db';
		$this->static_data   = 'iso3166.csv';
		$this->table_name    = 'countries';
	
		$this->db_loc   = Config::utilitiesDbDirectory() . '/' . $this->info_database;
		$this->data_loc = Config::utilitiesDataDirectory() . '/' . $this->static_data;
		$this->aux_loc  = Config::utilitiesDataDirectory() . '/' . $this->aux_list;
			
		if (! $this->initialize($force)) {
			throw new Exception('Object creation failed. Database ' . $this->info_database . ' corrupt or incomplete');
		}
	}
	
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// public methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	
	public function retrieveUsing($term, $value) { return $this->retrieve_code_using($term, $value); }
	// ----------------------------------------------------------------------
	// countryCodeExists
	// ----------------------------------------------------------------------
	
	public function countryCodeExists($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return false; }
		return true;
	}

	// ----------------------------------------------------------------------
	// countryNameExists
	// ----------------------------------------------------------------------
	
	public function countryNameExists($name) {
		if ($this->retrieveCountryCode($name)) { return true; }
		return false;
	}

	// ----------------------------------------------------------------------
	// allCountryCodes
	// ----------------------------------------------------------------------
	
	public function allCountryCodes() {
		$out = array();
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    $out = array();
	    try {
	    	$q = $this->db->prepare($sql);
	 		$q->execute();
	   		while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	   			$out[] = $res['iso3166_a2'];
	   		}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving country codes (' . $e->getMessage() . ')', E_ERROR);
			return null;
		}
		
		return $out;
	}

	// ----------------------------------------------------------------------
	// allAlpha3Codes
	// ----------------------------------------------------------------------
	
	public function allAlpha3Codes() {
		$out = array();
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    $out = array();
	    try {
	    	$q = $this->db->prepare($sql);
	 		$q->execute();
	   		while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	   			$out[] = $res['iso3166_a3'];
	   		}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving numeric codes (' . $e->getMessage() . ')', E_ERROR);
			return null;
		}
		
		return $out;
	}	

	// ----------------------------------------------------------------------
	// allNumericCodes
	// ----------------------------------------------------------------------
	
	public function allNumericCodes() {
		$out = array();
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    $out = array();
	    try {
	    	$q = $this->db->prepare($sql);
	 		$q->execute();
	   		while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	   			$out[] = $res['iso3166_num'];
	   		}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving numeric codes (' . $e->getMessage() . ')', E_ERROR);
			return null;
		}
		
		return $out;
	}	

	// ----------------------------------------------------------------------
	// allTerritories
	// ----------------------------------------------------------------------
	
	public function allTerritories() {
		$out = array();
		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE independent=0;';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    $out = array();
	    try {
	    	$q = $this->db->prepare($sql);
	 		$q->execute();
	   		while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	   			$out[] = $res['iso3166_a2'];
	   		}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving country codes (' . $e->getMessage() . ')', E_ERROR);
			return null;
		}
		
		return $out;
	}

	// ----------------------------------------------------------------------
	// allIndependentNations
	// ----------------------------------------------------------------------
	
	public function allIndependentNations() {
		$out = array();
		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE independent=1;';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    $out = array();
	    try {
	    	$q = $this->db->prepare($sql);
	 		$q->execute();
	   		while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	   			$out[] = $res['iso3166_a2'];
	   		}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving country codes (' . $e->getMessage() . ')', E_ERROR);
			return null;
		}
		
		return $out;
	}
	
	// ----------------------------------------------------------------------
	// retrieveCountryData
	// ----------------------------------------------------------------------

	public function getCountryData($cc, $asJSON = null) { return $this->retrieveCountryData($cc); }
	
	public function retrieveCountryData($cc, $asJSON = null) {
		$data = $this->retrieve_data($cc);
		if ($asJSON) { return json_encode($data); }
		
		return $data;
	}

	// ----------------------------------------------------------------------
	// retrieveCountryDataUsingAlpha3Code
	// ----------------------------------------------------------------------

	public function getCountryDataUsingAlpha3Code($alpha3) { 
		return $this->retrieveCountryDataUsingAlpha3Code($alpha3);
	}
	
	public function retrieveCountryDataUsingAlpha3Code($alpha3) {
		$cc2 = $this->retrieve_code_using('iso3166_a3', $alpha3);
		return $this->retrieve_data($cc2);
	}

	// ----------------------------------------------------------------------
	// retrieveCountryDataUsingNumericCode
	// ----------------------------------------------------------------------

	public function getCountryDataUsingNumericCode($num) { 
		return $this->retrieveCountryDataUsingNumericCode($num);
	}
	
	public function retrieveCountryDataUsingNumericCode($num) {
		$cc2 = $this->retrieve_code_using('iso3166_num', $num);
		return $this->retrieve_data($cc2);
	}
	
	// ----------------------------------------------------------------------
	// retrieveCountryCode
	// ----------------------------------------------------------------------

	public function getCountryCode($name) { return $this->retrieveCountryCode($name); }
		
	public function retrieveCountryCode($name) {
		return $this->retrieve_code_using('iso3166_name', $name);
	}
	
	// ----------------------------------------------------------------------
	// retrieveCountryName
	// ----------------------------------------------------------------------
	
	public function getCountryName($cc) { return $this->retrieveCountryName($cc); }
	
	public function retrieveCountryName($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return null; }
		return $data['iso3166_name'];
	}

	// ----------------------------------------------------------------------
	// retrieveNumericCode
	// ----------------------------------------------------------------------
	
	public function getNumericCode($cc) { return $this->retrieveNumericCode($cc); }
	
	public function retrieveNumericCode($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return false; }
		return $data['iso3166_num'];
	}

	// ----------------------------------------------------------------------
	// retrieveAlpha3Code
	// ----------------------------------------------------------------------
	
	public function getAlpha3Code($cc) { return $this->retrieveAlpha3Code($cc); }
	
	public function retrieveAlpha3Code($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return false; }
		return $data['iso3166_cc3'];
	}
	
	// ----------------------------------------------------------------------
	// retrieveCountryCapital
	// ----------------------------------------------------------------------
	
	public function getCountryCapital($cc) { return $this->retrieveCountryCapital($cc); }
	
	public function retrieveCountryCapital($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return null; }
		return $data['capital']; 
	}

	// ----------------------------------------------------------------------
	// retrieveInternetTLDForCountryCode
	// ----------------------------------------------------------------------
	
	public function getInternetTLDForCountryCode($cc) {
		return $this->retrieveInternetTLDForCountryCode($cc);
	}
	
	public function retrieveInternetTLDForCountryCode($cc) {
		if (! $this->countryCodeExists($cc)) { return null; }
		
		$data = $this->retrieve_data($cc);
		if (! $data) { return null; }
		return $data['tld'];
	}

	// ----------------------------------------------------------------------
	// callingCodesForCountryCode/ITUCodesForCountryCode
	// ----------------------------------------------------------------------
	
	public function callingCodesForCountryCode($cc) {
		return $this->ITUCodesForCountryCode($cc);
	}
		
	public function ITUCodesForCountryCode($cc) {
		if (! $this->countryCodeExists($cc)) { return null; }
		
		$data = $this->retrieve_data($cc);
		if (! $data) { return null; }
		return json_decode($data['itu_codes'], true);
	}
	
	// ----------------------------------------------------------------------
	// isUNMember
	// ----------------------------------------------------------------------
	
	public function isUNMember($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return false; }
		
		if ($data['un_member'] != '0') { return true; }
		return false; 
	}

	// ----------------------------------------------------------------------
	// isIndependent
	// ----------------------------------------------------------------------
	
	public function isIndependent($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return null; }
		
		if (strval($data['independent']) != '0') { return true; }
		return false; 
	}
	
	// ----------------------------------------------------------------------
	// hasCapital
	// ----------------------------------------------------------------------
	
	public function hasCapital($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return false; }
		if (($data['capital'] == 'None') || ($data['capital'] == '')) { return false; }
	
		return true;
	}	

	// ----------------------------------------------------------------------
	// territoryOf
	// proper use:
	// if (! $obj->isIndependent($cc)) { $country = $obj->territoryOf($cc); }
	// ----------------------------------------------------------------------
	
	public function territoryOf($cc) {
		$data = $this->retrieve_data($cc);
		if (! $data) { return null; }
		
		if (strval($data['independent']) != '0') { 
			return null;  // an independent state
		} else {
			$tcc = $this->territories[$cc];
			if (! $tcc) {
				print "MISMATCH: no territory for $cc (independence: " . $data['independent'] . ")\n";
				Utilities::logger("MISMATCH: no territory for $cc (independence: " . $data['independent'], E_WARNING);
				return null;
			} else {
				return $this->territories[$cc];  // a territory
			} 
		}
	}
	
	// ----------------------------------------------------------------------
	// retrieveCountryNamesLike
	// ----------------------------------------------------------------------
	
	public function retrieveCountryNamesLike($str) {
		$items = $this->retrieveInfoLike('iso3166_name', utf8_encode($str));
		if (! $items) { return null; }
		$match = array();
		foreach ($items as $item) {
			$match[utf8_decode($item['iso3166_name'])] = $item['iso3166_a2'];
		}
		
		return $match;
	}

	// ----------------------------------------------------------------------
	// retrieveCountryCodeFuzzy
	// ----------------------------------------------------------------------
		
	public function retrieveCountryCodeFuzzy($name) {
		$sql = 'SELECT iso3166_a2 FROM ' . $this->table_name . ' WHERE iso3166_name LIKE \'%' . $name . '%\';';	
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    	$codes = array();
	    	while (($res = $q->fetch(PDO::FETCH_ASSOC)) != null) {
	    		$codes[] = $res['iso3166_a2'];
			}
		    return $codes;
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving country code for patter [' . $name . ']', E_USER_NOTICE);
		}
		return null;
	}
	
	// ----------------------------------------------------------------------
	// rebuild
	// ----------------------------------------------------------------------

	public function rebuild() {
		if (! $this->load_table()) {
			throw new Exception('could not load ISO3166 Country database table ' . $this->table_name);
		}

		if (! $this->update_auxiliary_info()) {
			throw new Exception('could not update auxiliary information in database table ' . $this->table_name);
		}

		Utilities::logger('countries table rebuilt', E_NOTICE);
		return true;
	}
	
	// ----------------------------------------------------------------------
	// dump
	// ----------------------------------------------------------------------

	public function dump($to_file = null) {
		if ($to_file == null) {
			$ran = substr(sha1(time()), 0, 12);
			$to_file = tempnam(sys_get_temp_dir(), 'country_code_backup_' . $ran);
		}
		$fp = fopen($to_file, "w");
		if (! $fp) {
			Utilities::logger("dump: could not open " . $to_file . " for writing", E_WARNING);
			return false;
		}	

		fputcsv($fp, array('iso3166_a2', 'iso3166_num', 'iso3166_a3', 'iso3166_name', 'tld', 'independent', 'un_member','capital','itu_codes'));	// write CSV header
		
		$sql = 'SELECT * FROM ' . $this->table_name . ';';
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);
	    
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    	while ($res = $q->fetch(PDO::FETCH_ASSOC)) {
	    		fputcsv($fp, array(
	    			$res['iso3166_a2'],
	    			sprintf('%03d', $res['iso3166_num']),
	    			$res['iso3166_a3'],
	    			$res['iso3166_name'],
	    			$res['tld'],
	    			$res['independent'],
	    			$res['un_member'],
	    			$res['capital'],
	    			$res['itu_codes']
	    		));
	    	}
	    } catch (PDOException $e) {
			Utilities::logger('PDO error dumping country data (' . $e->getMessage() . ')', E_ERROR);
			return false;
		}		

		fclose($fp);
		Utilities::logger("contents of " . $this->table_name . " dumped to " . $to_file, E_NOTICE);		
		return true;		
	}
		
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// private methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// update_auxiliary_info
	// ----------------------------------------------------------------------

	private function update_auxiliary_info() {
		$fcc = fopen($this->aux_loc, 'r');
		if (! $fcc) {
			Utilities::logger("error opening static auxiliary data file " . $this->aux_loc, E_ERROR);
			return false;
		}
		fgetcsv($fcc, 200, ','); // strip header
		while ($vals = fgetcsv($fcc, 400, ',')) {
			$code  = $vals[0];
			
			$tld   = $vals[1];
			$indep = $vals[3];
			$un    = $vals[3];
			$cap   = $vals[4];
			$itu   = json_decode($vals[5], true);
			$this->update_auxiliary_data($code, $tld, $indep, $un, $cap, $itu);
		}
		Utilities::logger('completed loading auxiliary data to countries table', E_NOTICE);		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// update_auxiliary_data
	// ----------------------------------------------------------------------

	private function update_auxiliary_data($code, $tld, $indep, $un, $capital, $itu_codes) {
	    $sql = 'UPDATE ' . $this->table_name . ' SET tld=?,independent=?,un_member=?,capital=?,itu_codes=? WHERE iso3166_a2=?;';
	    Utilities::logger("PDO stmt: " . $sql, E_USER_NOTICE);
	    
	    switch (strtolower($indep)) {
	    	case 'yes':
	    	case '1':
	    		$i_value = 1;
	    		break;
	    	case 'no':
	    	case '0':
	    	default:
	    		$i_value = 0;
	    }

	    switch (strtolower($un)) {
	    	case 'yes':
	    	case '1':
	    		$u_value = 1;
	    		break;
	    	case 'no':
	    	case '0':
	    	default:
	    		$u_value = 0;
	    }
	    
	    try {
			$q = $this->db->prepare($sql);
			$q->execute(array($tld, $i_value, $u_value, $capital, json_encode($itu_codes), $code));
		} catch (PDOException $e) {
			Utilities::logger('PDO: update_auxiliary_data failed to insert values (' . $e->getMessage() . ')', E_ERROR);      
			return false;
		}
		
		return true;
	}

	// ----------------------------------------------------------------------
	// retrieve_data
	// ----------------------------------------------------------------------
		
	private function retrieve_data($cc) {
		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE iso3166_a2=?';	
		Utilities::logger("PDO query: " . $sql . " with [" . $cc . "]", E_USER_NOTICE);
		
	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute(array(strtoupper($cc)));
	    	$res = $q->fetch(PDO::FETCH_ASSOC);
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving country code [' . $cc . ']: ' . $e->getMessage(), E_USER_NOTICE);
			return null;
		}

		if ($res['itu_codes'] != null) {
			$res['itu_codes'] = json_decode($res['itu_codes'], true);
		}
		
		return $res;
	}

	// ----------------------------------------------------------------------
	// retrieve_code_using (iso3166_a2, iso3166_a3, iso3166_num, iso3166_name)
	// (use of LIKE allows for case-independent SELECT match)
	// ----------------------------------------------------------------------
		
	private function retrieve_code_using($field, $term) {
		if (! in_array($field, array('iso3166_a2', 'iso3166_a3', 'iso3166_num','iso3166_name'))) {
			throw new Exception('improper query field [' . $field . ']');
		}
		
		if (($field == 'iso3166_a2') || ($field == 'iso3166_a3')) { $term = strtoupper($term); }
	    if ($field == 'iso3166_name') { 
	    	if (strpos($term, '\'')) { $term = substr($term, 0, strpos($term, '\'')) .  '\'' . strstr($term, '\''); }
	    	$term = utf8_encode($term);
	    }

		$sql = 'SELECT * FROM ' . $this->table_name . ' WHERE ' . $field . ' LIKE "%' . $term . '%"';	
		Utilities::logger("PDO query: " . $sql, E_USER_NOTICE);

	    try {
	    	$q = $this->db->prepare($sql);
	    	$q->execute();
	    	$res = $q->fetch(PDO::FETCH_ASSOC);
	    } catch (PDOException $e) {
			Utilities::logger('PDO error retrieving [' . $field . '] using [' . $term . ']: ' . $e->getMessage(), E_USER_NOTICE);
			return null;
		}
		
		if (!$res) { return null; }
		return $res['iso3166_a2']; 
	}				

	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------
	// protected methods
	// ----------------------------------------------------------------------
	// ----------------------------------------------------------------------

	// ----------------------------------------------------------------------
	// load_table
	// ----------------------------------------------------------------------

	protected function load_table() {
		$fcc = fopen($this->data_loc, 'r');
		if (! $fcc) {
			Utilities::logger("error opening static country data file " . $this->data_loc, E_ERROR);
			return false;
		}
		//unload current items
		if (! $this->unload_table()) {
			return false;
		}
		
		$num = 0;
		fgetcsv($fcc, 200, ','); // strip header
		while ($vals = fgetcsv($fcc, 400, ',')) {
			if (preg_match('/[^\x20-\x7f]/', $vals[0])) {
				print "non ASCII characters in name [" . $vals[0] . "]\n";
			}
			// ITU codes are a "flattened" JSON string in external file and on disk
			$this->load_entry($vals[0], $vals[1], $vals[2], $vals[3]);
			$num++;
		}
		fclose($fcc);
		Utilities::logger($num . ' country entries added to country table', E_NOTICE);

		if (! $this->update_auxiliary_info()) { return false; }		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// load_entry
	// ----------------------------------------------------------------------

	protected function load_entry($code, $num, $code3, $name, $tld = null, $indep = 0, $member = 0, $capital = null, $itu_codes = null) {
		if (strpos($name, '\'')) { $name = substr($name, 0, strpos($name, '\'')) .  '\'' . strstr($name, '\''); }
		
	    $sql = 'INSERT OR REPLACE INTO ' . $this->table_name . ' VALUES (?,?,?,?,?,?,?,?,?)';
	    Utilities::logger("PDO stmt: " . $sql, E_USER_NOTICE);
	    
	    try {
			$q = $this->db->prepare($sql);
			$q->execute(array(strtoupper($code), sprintf('%03d', $num), $code3, $name, $tld, $indep, $member, $capital, $itu_codes));
		} catch (PDOException $e) {
			Utilities::logger('PDO: load_entry failed to insert values (' . $e->getMessage() . ')', E_ERROR);      
			return false;
		}
		
		return true;
	}
	
	// ----------------------------------------------------------------------
	// create_table
	// ----------------------------------------------------------------------

	protected function create_table() {
		$sql = 'CREATE TABLE ' . $this->table_name . ' (iso3166_a2 CHARACTER(2) PRIMARY KEY, iso3166_num CHARACTER(3), iso3166_a3 CHARACTER(3), iso3166_name TEXT, tld CHARACTER(4), independent BOOLEAN, un_member BOOLEAN, capital TEXT, itu_codes TEXT);';
		Utilities::logger("PDO stmt: " . $sql, E_NOTICE);
		
		try {
			$this->db->exec($sql);
		} catch (PDOException $e) {
	    	Utilities::logger("PDO error creating database table in " . $this->db_loc . ' is ' . $e->getMessage(), E_ERROR);
	    	return false;
	    }
	    
	    return true;
	}
/** 
 * ----------------------------------------------------------------------
 * END
 * ----------------------------------------------------------------------
 **/	
}
?>